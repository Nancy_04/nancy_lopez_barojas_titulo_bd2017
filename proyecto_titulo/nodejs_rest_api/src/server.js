// server.js

// base setup

var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/modelo')
var Modelo = require('./app/models/modelo');
var Cliente = require('./app/models/cliente');
var Proveedor = require('./app/models/proveedor');
var Medidas = require('./app/models/medidas');

// call the packages


var express = require('express')
var app = express();
var bodyParser = require('body-parser');

// config app to use bodyParser()

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8081; 

// routes for our api

var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('something is happen..');
  

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
  
  res.header('Access-Control-Allow-Headers', 'Content-Type');


	next();  // make sure we go to the next routes
});


// test route

router.get('/', function(req, res) {
 res.json({ message: 'yahoo!! welcome to our api !' });
});

// more routes for our API will happen here
router.route('/modelo')
 
  // create a bear accessed at POST
  // http://localhost:8081/api/bears
 .post(function (req, res) {
 	var modelo = new Modelo();
	modelo.tip = req.body.tip;
	modelo.tel = req.body.tel;
	modelo.col = req.body.col;
	modelo.pre = req.body.pre


 	// save the bear and check for errors
 	modelo.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Modelo created !'});
 	});

 	// body...
 })
  

// more routes for our API will happen here
router.route('/cliente')
 
  // create a bear accessed at POST
  // http://localhost:8081/api/bears
 .post(function (req, res) {
 	var cliente = new Cliente();
	cliente.nom = req.body.nom;
	cliente.tel = req.body.tel;
	cliente.dic = req.body.dic;
	cliente.sal = req.body.cor


 	// save the bear and check for errors
 	cliente.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Cliente created !'});
 	});

 	// body...
 })

// more routes for our API will happen here
router.route('/proveedor')
 
  // create a bear accessed at POST
  // http://localhost:8081/api/bears
 .post(function (req, res) {
 	var proveedor = new Proveedor();
	proveedor.nom = req.body.nom;
	proveedor.tel = req.body.tel;
	proveedor.dic = req.body.dic;
	proveedor.cor = req.body.cor


 	// save the bear and check for errors
 	proveedor.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Proveedor created !'});
 	});

 	// body...
 })





// more routes for our API will happen here
router.route('/medidas')
 
  // create a bear accessed at POST
  // http://localhost:8081/api/bears
 .post(function (req, res) {
 	var medidas = new Medidas();
	medidas.esp = req.body.esp;
	medidas.bus = req.body.bus;
	medidas.cin = req.body.cin;
	medidas.cad = req.body.cad


 	// save the bear and check for errors
 	medidas.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Medidas created !'});
 	});

 	// body...
 })



 // get all the bears (accessed at 
 // http://localhost:8081/api/bears)
 .get(function (req, res) {

 	// body...
 	Modelo.find(function(err, modelo) 
 	{
 		if (err)
 			res.send(err);
 		res.json(modelo);
 	});
 });

// on routes that end in /bears/:bear_id
router.route('/modelo/:modelo_id')
 // get the bear with that id 
 // accessed at GET 
 // http://localhost:8081/api/bears/:bear_id

 .get(function(req, res) {
 	// body...
 	Modelo.findById(req.params.modelo_id, function(err, modelo)
 	{
 		if (err)
 			res.send(err);
 		res.json(modelo);

 	});
  })






// get all the bears (accessed at 
 // http://localhost:8081/api/bears)
 .get(function (req, res) {

 	// body...
 	Cliente.find(function(err, cliente) 
 	{
 		if (err)
 			res.send(err);
 		res.json(cliente);
 	});
 });

// on routes that end in /bears/:bear_id
router.route('/cliente/:cliente_id')
 // get the bear with that id 
 // accessed at GET 
 // http://localhost:8081/api/bears/:bear_id

 .get(function(req, res) {
 	// body...
 	Cliente.findById(req.params.cliente_id, function(err, cliente)
 	{
 		if (err)
 			res.send(err);
 		res.json(cliente);

 	});
  })




// get all the bears (accessed at 
 // http://localhost:8081/api/bears)
 .get(function (req, res) {

 	// body...
 	Proveedor.find(function(err, proveedor) 
 	{
 		if (err)
 			res.send(err);
 		res.json(proveedor);
 	});
 });

// on routes that end in /bears/:bear_id
router.route('/proveedor/:proveedor_id')
 // get the bear with that id 
 // accessed at GET 
 // http://localhost:8081/api/bears/:bear_id

 .get(function(req, res) {
 	// body...
 	Proveedor.findById(req.params.proveedor_id, function(err, proveedor)
 	{
 		if (err)
 			res.send(err);
 		res.json(proveedor);

 	});
  })







// get all the bears (accessed at 
 // http://localhost:8081/api/bears)
 .get(function (req, res) {

 	// body...
 	Medidas.find(function(err, medidas) 
 	{
 		if (err)
 			res.send(err);
 		res.json(medidas);
 	});
 });

// on routes that end in /bears/:bear_id
router.route('/medidas/:medidas_id')
 // get the bear with that id 
 // accessed at GET 
 // http://localhost:8081/api/bears/:bear_id

 .get(function(req, res) {
 	// body...
 	Medidas.findById(req.params.medidas_id, function(err, medidas)
 	{
 		if (err)
 			res.send(err);
 		res.json(medidas);

 	});
  })

// update the bear with this id
 // accessed at PUT
 // http://localhost:8081/api/bears/:bear_id

 .put(function (req, res) {
 	// use our bear model to find the bear we want
 	Modelo.findById(req.params.modelo_id, function(err, modelo) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		modelo.tip = req.body.tip;
 		modelo.tel = req.body.tel;
 		modelo.col = req.body.col;
    		modelo.pre = req.body.pre;

 		// save the bear

 		modelo.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Modelo updated !'});
 		});
 	});
  })




// update the bear with this id
 // accessed at PUT
 // http://localhost:8081/api/bears/:bear_id

 .put(function (req, res) {
 	// use our bear model to find the bear we want
 	Cliente.findById(req.params.cliente_id, function(err, cliente) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		cliente.nom = req.body.nom;
 		cliente.tel = req.body.tel;
 		cliente.dic = req.body.dic;
    		cliente.cor = req.body.cor;

 		// save the bear

 		cliente.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Cliente updated !'});
 		});
 	});
  })



// update the bear with this id
 // accessed at PUT
 // http://localhost:8081/api/bears/:bear_id

 .put(function (req, res) {
 	// use our bear model to find the bear we want
 	Proveedor.findById(req.params.proveedor_id, function(err, proveedor) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		proveedor.nom = req.body.nom;
 		proveedor.tel = req.body.tel;
 		proveedor.dic = req.body.dic;
    		proveedor.cor = req.body.cor;

 		// save the bear

 		proveedor.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Proveedor updated !'});
 		});
 	});
  })




// update the bear with this id
 // accessed at PUT
 // http://localhost:8081/api/bears/:bear_id

 .put(function (req, res) {
 	// use our bear model to find the bear we want
 	Medidas.findById(req.params.medidas_id, function(err, medidas) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		medidas.esp = req.body.esp;
 		medidas.bus = req.body.bus;
 		medidas.cin = req.body.cin;
    		medidas.cad = req.body.cad;

 		// save the bear

 		medidas.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Medidas updated !'});
 		});
 	});
  })








 // delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8081/api/bears/:bear_id
 delete(function (req, res) {
   Modelo.remove({
   	_id : req.params.modelo_id
   }, function (err, modelo) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Modelo deleted !'});
   });

 });

 
// delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8081/api/bears/:bear_id
 delete(function (req, res) {
   Cliente.remove({
   	_id : req.params.cliente_id
   }, function (err, cliente) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Cliente deleted !'});
   });

 });

 // delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8081/api/bears/:bear_id
 delete(function (req, res) {
   Proveedor.remove({
   	_id : req.params.proveedor_id
   }, function (err, proveedor) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Proveedor deleted !'});
   });

 });



// delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8081/api/bears/:bear_id
 delete(function (req, res) {
   Medidas.remove({
   	_id : req.params.medidas_id
   }, function (err, medidas) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Medidas deleted !'});
   });

 });


// register our routes
app.use('/api', router);
//app.use(require('cors')());

// start the server

app.listen(port);
console.log('Magic happens on port: ' + port);

//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProveedorSchema = new Schema({
	nom : String,
	tel : String,
	dic : String,
	cor : String
});

module.exports = mongoose.model('Proveedor', ProveedorSchema);

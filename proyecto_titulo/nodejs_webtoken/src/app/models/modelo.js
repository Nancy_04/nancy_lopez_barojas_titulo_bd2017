//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ModeloSchema = new Schema({
	tip : String,
	tel : String,
	col : String,
	pre : String
});

module.exports = mongoose.model('Modelo', ModeloSchema);

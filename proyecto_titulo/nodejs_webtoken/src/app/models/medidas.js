//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MedidasSchema = new Schema({
	esp : String,
	bus : String,
	cin : String,
	cad : String
});

module.exports = mongoose.model('Medidas', MedidasSchema);

// get the packages we need
var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());

var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');

var jwt = require('jsonwebtoken');
var config = require('./config');


var User = require('./app/models/user');
var Modelo = require('./app/models/modelo');
var Proveedor = require('./app/models/proveedor');
var Cliente = require('./app/models/cliente');
var Medidas = require('./app/models/medidas');

// configuration

var port = process.env.PORT || 8082;
mongoose.connect(config.database);
app.set('superSecret', config.secret);

// use body parser so we can get info from POST
// and/or URL parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

//
// routes
//

app.get('/', function (req, res) {
	res.send('Hello! the API is at http://localhost:' + port + '/api');
});

// add demo user

app.get('/setup', function (req, res) {
	// create a sample user
	var nick = new User({
		name : 'adsoft',
		password: 'qubit',
		admin: true
	});

	// save the sample user

	nick.save(function(err) {
		if (err) throw err;

		console.log('User saved suscessfully');
		res.json({success: true});	
	});
});

// api routes

// get an instance of the router for api routes
var apiRoutes = express.Router();

// route to authenticate a user

apiRoutes.post('/authenticate', function (req, res) {
 
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    //res.setHeader('Access-Control-Allow-Credentials', true); // If needed

	//find the user
	User.findOne({name: req.body.name}, function (err, user) 
	{
		if (err) throw err;
	
		if (!user) {
			res.json({success: false, message: 
				'Authentication failed, User not found'});
		}
		else if (user) {
			// check if password matches
			if (user.password != req.body.password) {
				res.json({success : false, message : 
					'Authentication failed, wrong password'});
			}
			else {
				// user and password is right
				var token = jwt.sign(user, app.get('superSecret'), {
					expiresIn: 200 * 60
				});

				// return the information including token as JSON

				res.json({
					success : true,
					message : 'Enjoy your token !',
					token : token
				});
				
			}
		}

		
	});
});

// route to middleware to verify a token

apiRoutes.use(function (req, res, next) {
	// check header or url parameters or post parameters for token
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    //res.setHeader('Access-Control-Allow-Credentials', true); // If needed


	var token = req.body.token ||
	 			req.query.token ||
	 			req.headers['x-access-token'];
	// decode token
	if (token)	{
		// verifies secret and checks up
		jwt.verify(token, app.get('superSecret'), function (err, decoded) {
			if (err) {
				return res.json({success: false, message : 'Failed to authenticate token' });

			} else {
				// is everything is good, save to request for use in other routes
				req.decoded = decoded;
				next();
			}
		});
	}else {
		// if there is not token, return an error

		return res.status(403).send( {
			success: false,
			message: 'No token provided'
		});
	}
});


// route to show a random message

apiRoutes.get('/', function (req, res) {
	res.json({message: 'welcome to the coolest API on the heart'});
});

// route to return all users

apiRoutes.get('/users', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	User.find({}, function(err, users) {
		res.json(users);
	});
});



// route to return all bears

apiRoutes.get('/modelo', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	Modelo.find({}, function(err, modelo) {
		res.json(modelo);
	});
});


// route to return all bears

apiRoutes.get('/cliente', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	Cliente.find({}, function(err, cliente) {
		res.json(cliente);
	});
});


// route to return all bears

apiRoutes.get('/proveedor', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	Proveedor.find({}, function(err, proveedor) {
		res.json(proveedor);
	});
});




// route to return all bears

apiRoutes.get('/medidas', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	Medidas.find({}, function(err, medidas) {
		res.json(medidas);
	});
});




apiRoutes.get('/modelo/:modelo_id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	// body...
 	Modelo.findById(req.params.modelo_id, function(err, modelo)
 	{
 		if (err)
 			res.send(err);
 		res.json(modelo);

 	});
  
});

apiRoutes.get('/cliente/:cliente_id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	// body...
 	Cliente.findById(req.params.cliente_id, function(err, cliente)
 	{
 		if (err)
 			res.send(err);
 		res.json(cliente);

 	});
  
});

apiRoutes.get('/proveedor/:proveedor_id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	// body...
 	Proveedor.findById(req.params.proveedor_id, function(err, proveedor)
 	{
 		if (err)
 			res.send(err);
 		res.json(proveedor);

 	});
  
});


apiRoutes.get('/medidas/:medidas_id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	// body...
 	Medidas.findById(req.params.medidas_id, function(err, medidas)
 	{
 		if (err)
 			res.send(err);
 		res.json(medidas);

 	});
  
});





 // create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.post('/modelo', function (req, res) {
 	var modelo = new Modelo();
 	modelo.tip = req.body.tip;
 	modelo.tel = req.body.tel;
 	modelo.col = req.body.col;
 	modelo.pre = req.body.pre;


 	// save the bear and check for errors
 	modelo.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Modelo created !'});
 	});

 	// body...
 });

// create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.post('/cliente', function (req, res) {
 	var cliente = new Cliente();
 	cliente.nom = req.body.nom;
 	cliente.tel = req.body.tel;
 	cliente.dic = req.body.dic;
 	cliente.cor = req.body.cor;


 	// save the bear and check for errors
 	cliente.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Cliente created !'});
 	});

 	// body...
 });

// create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.post('/proveedor', function (req, res) {
 	var proveedor = new Proveedor();
 	proveedor.nom = req.body.nom;
 	proveedor.tel = req.body.tel;
 	proveedor.dic = req.body.dic;
 	proveedor.cor = req.body.cor;


 	// save the bear and check for errors
 	proveedor.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Proveedor created !'});
 	});

 	// body...
 });






// create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.post('/medidas', function (req, res) {
 	var medidas = new Medidas();
 	medidas.esp = req.body.esp;
 	medidas.bus = req.body.bus;
 	medidas.cin = req.body.cin;
 	medidas.cad = req.body.cad;


 	// save the bear and check for errors
 	medidas.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'Medidascreated !'});
 	});

 	// body...
 });


 // create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.put('/modelo/:modelo_id', function (req, res) {
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	

 	var modelo = new Modelo();
 	
 	
 	Modelo.findById(req.params.modelo_id, function(err, modelo) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		modelo.tip = req.body.tip;
 		modelo.tel = req.body.tel;
 		modelo.col = req.body.col;
 		modelo.pre = req.body.pre;



 		// save the bear

 		modelo.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Modelo updated !'});
 		});
 	});
 	// body...
 });

// create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.put('/cliente/:cliente_id', function (req, res) {
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	

 	var cliente = new Cliente();
 	
 	
 	Cliente.findById(req.params.cliente_id, function(err, cliente) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		cliente.nom = req.body.nom;
 		cliente.tel = req.body.tel;
 		cliente.dic = req.body.dic;
 		cliente.cor = req.body.cor;



 		// save the bear

 		cliente.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Cliente updated !'});
 		});
 	});
 	// body...
 });


// create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.put('/proveedor/:proveedor_id', function (req, res) {
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	

 	var proveedor = new Proveedor();
 	
 	
 	Proveedor.findById(req.params.proveedor_id, function(err, proveedor) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		proveedor.nom = req.body.nom;
 		proveedor.tel= req.body.tel;
 		proveedor.dic = req.body.dic;
 		proveedor.cor = req.body.cor;



 		// save the bear

 		proveedor.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Proveedor updated !'});
 		});
 	});
 	// body...
 });





// create a new bear accessed at POST
  // http://localhost:8082/api/bears
apiRoutes.put('/medidas/:medidas_id', function (req, res) {
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	

 	var medidas = new Medidas();
 	
 	
 	Medidas.findById(req.params.medidas_id, function(err, medidas) {
 		if (err)
 			res.send(err);

 		// update the bears info
 		medidas.esp = req.body.esp;
 		medidas.bus = req.body.bus;
 		medidas.dic = req.body.dic;
 		medidas.cad = req.body.cad;



 		// save the bear

 		medidas.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'Medidas updated !'});
 		});
 	});
 	// body...
 });







// delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8082/api/bears/:bear_id
apiRoutes.delete('/modelo/:modelo_id', function (req, res) {

   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
   Modelo.remove({
   	_id : req.params.modelo_id
   }, function (err, modelo) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Modelo deleted !'});
   });

 });


// delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8082/api/bears/:bear_id
apiRoutes.delete('/cliente/:cliente_id', function (req, res) {

   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
   Cliente.remove({
   	_id : req.params.cliente_id
   }, function (err, cliente) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Cliente deleted !'});
   });

 });


 // delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8082/api/bears/:bear_id
apiRoutes.delete('/proveedor/:proveedor_id', function (req, res) {

   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
   Proveedor.remove({
   	_id : req.params.proveedor_id
   }, function (err, proveedor) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Proveedor deleted !'});
   });

 });




 // delete the bear with this id 
 // accessed at DELETE
 // http://localhost:8082/api/bears/:bear_id
apiRoutes.delete('/medidas/:medidas_id', function (req, res) {

   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
   Medidas.remove({
   	_id : req.params.medidas_id
   }, function (err, medidas) {
   	if (err)
   		res.send(err);
   	res.json({message: 'Medidas deleted !'});
   });

 });
 

//var cors = require('cors');

// use it before all route definitions
//app.use(cors({origin: 'http://localhost:8083'}));
// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);
app.disable('etag');


// start the server
app.listen(port);
console.log('Magic happens with web-token, on port : ' + port);
